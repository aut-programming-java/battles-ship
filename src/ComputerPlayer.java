import java.util.Random;

/**
 * This inheritance Class player
 * and for time we have a computer player
 *
 * @autor Alireza Mazochi
 * @since  97/01/6 (Happy new year!)
 * @version 1.0.0
 */
public class ComputerPlayer extends Player {

    /**
     * This method for creating ships in start of every games
     */
    @Override
    public void createShips() {
        Random random=new Random();
        int x;
        int y;
        int length;
        boolean direction;
        typeOfShoot=random.nextBoolean();
        for(int i=0 ; i<Player.NUM_OF_SHIPS ; i++){
            x=random.nextInt(10);
            y=random.nextInt(10);
            length=random.nextInt(4)+2;
            direction=random.nextBoolean();
            Ship ship=new Ship(direction,x,y,length);
            if(checkAddShip(ship)==false){
                ship=null;
                i--;
            }
            else{
                ships[i]=ship;
                for(int j=0 ; j<length ; j++) {
                    if (direction == true) {
                        tableOwn[x][y+j]=1;
                    }
                    else {
                        tableOwn[x+j][y]=1;
                    }
                }
            }

        }
    }

    /**
     * this method for attacking to enemy player
     * @param opponent the enemy player
     * @return if the player success return true
     */
    @Override
    public boolean attack(Player opponent) {
        Random random=new Random();
        while(true){
            int x=random.nextInt(10);
            int y=random.nextInt(10);
            if(0<=x && x<=9 && 0<=y && y<=9 && tableEnemy[x][y]==0){
                int locationX;
                int locationY;
                while(true){
                    if(typeOfShoot){
                        locationX=x+random.nextInt(3)-1;
                        locationY=y+random.nextInt(3)-1;
                        if(0<=locationX && locationX<=9 && 0<=locationY && locationY<=9 && tableEnemy[locationX][locationY]==0){
                            break;
                        }
                    }
                    else{
                        locationX=x;
                        locationY=y;
                        break;
                    }
                }
                if(opponent.shoot(locationX,locationY)==true){
                    tableEnemy[locationX][locationY]=2;
                    opponent.tableOwn[locationX][locationY]=2;
                    delay(1000);
                    return true;
                }
                else{
                    tableEnemy[locationX][locationY]=1;
                    delay(1000);
                    return false;
                }
            }
           /* else{
                System.out.println("ERROR::This location choice before!");
            }*/
        }
    }
}
