/**
 * This class provide dates for a Ship
 *
 *@autor Alireza Mazochi
 * @since  97/01/6 (Happy new year!)
 * @version 1.0.0
 */
public class Ship {
    private boolean direct; //true --> vertical  false--> horizontal
    private int locationX;
    private int locationY;
    private int length;
    private boolean state[];

    /**
     * A getter for length
     * @return length
     */
    public int getLength() {
        return length;
    }

    /**
     * A getter for locationX
     * @return locationX
     */
    public int getLocationX() {

        return locationX;
    }

    /**
     * A getter for locationY
     * @return locationY
     */
    public int getLocationY() {
        return locationY;
    }

    /**
     * A getter for direct
     * @return direct
     */
    public boolean getDirect(){
        return direct;
    }

    /**
     * Constructor for class Ship
     * @param direct this field related that ship is horizontal or vertical
     * @param locationX this field show x of the top most or the left most point of ship
     * @param locationY this field show y of the top most or the left most point of ship
     * @param length the length of the ship
     */
    public Ship(boolean direct, int locationX, int locationY, int length){
        this.direct=direct;
        this.length=length;
        this.locationX=locationX;
        this.locationY=locationY;
        state=new boolean[length];
        for(int i=0 ; i<length ; i++){
            state[i]=true;
        }

    }

    /**
     * When a ship is created maybe x or y or length of this ship not invalid
     * and this method specifies this case
     * @return value of the ship
     */
    public boolean check(){
        if(0<=locationY && locationY<=9 && 0<=locationX && locationX<=9 && 2<=length && length<=5){
            if(direct==true){
                if(locationY+length<=9){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                if(locationX+length<=9){
                    return true;
                }
                else{
                    return false;
                }
            }
        }
        else{
            return false;
        }
    }

    /**
     * This method gives a point and if this point is a part of the ship, this part damages.
     * @param x x of the shoot point
     * @param y y of the shoot point
     * @return if a shoot is effective return true
     */
    public boolean shoot(int x,int y){
        for(int i=0 ; i<length  ; i++){
            if(direct==true){
                if(locationX==x && locationY+i==y){
                    state[i]=false;
                    return true;
                }
            }
            else{
                if(locationX+i==x && locationY==y){
                    state[i]=false;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * If the ship has at lest one part that
     * no damaged it means ship is available
     *
     * @return if the ship is available or not
     */
    public boolean isAvailable(){
        for(int i=0 ; i<length ; i++){
            if(state[i]==true){
                return true;
            }
        }
        return false;
    }

}
