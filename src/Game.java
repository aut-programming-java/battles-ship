import java.util.Scanner;

/**
 * This class is a manager of the game
 * This class take turn to player
 * and check who is winner
 *
 * @autor Alireza Mazochi
 * @since  97/01/6 (Happy new year!)
 * @version 1.0.0
 */
public class Game {
    Player player1;
    Player player2;

    /**
     * Constructor for class Game
     * Every games needs to player
     */
    public Game(){
        createPlayers();
    }

    /**
     * This class gives type of games and gives details of player
     */
    public void createPlayers(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Choose a type of game:");
        System.out.println("1.two human players");
        System.out.println("2.A human player and a computer player");
        boolean flag=true;
        while(flag){
            int choice;
            choice=scanner.nextInt();
            flag=false;
            switch (choice){
                case 1:
                    System.out.println("Player1;answer to this question:");
                    player1=new HumanPlayer();
                    System.out.println("Player2;answer to this question:");
                    player2=new HumanPlayer();
                    break;
                case 2:
                    System.out.println("Player1;answer to this question:");
                    player1=new HumanPlayer();
                    player2=new ComputerPlayer();
                    break;
                default:
                    flag=true;
                    System.out.println("ERROR");
                    break;
            }
        }
    }

    /**
     * A method for delay
     * @param time Amount of time that we like program delays
     */
    public void delay(int time){
        try
        {
            Thread.sleep(time);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * This method use for start of playing
     */
    public void start(){
        boolean flag=true;
        Scanner scanner=new Scanner(System.in);
        while (flag){
            ///Turn:player 1
            if(flag){
                clearConsole();
                System.out.println("Turn Player 1");
                delay(3000);
                clearConsole();
            }
            while (flag){
                if(player1.attack(player2)==false){
                    break;
                }
                else if(player2.hasShip()==false){
                    flag=false;
                    System.out.println("player1 win");
                }
            }

            ///Turn:Player 2
            if(flag){
                clearConsole();
                System.out.println("Turn Player 2");
                delay(3000);
                clearConsole();
            }
            while (flag){
                if(player2.attack(player1)==false){
                    break;
                }
                else if(player1.hasShip()==false){
                    flag=false;
                    System.out.println("player2 win");
                }
            }
        }

    }

    /**
     * A simple method for clearing console
     */
    public void clearConsole(){
        System.out.flush();
        for(int i = 0 ; i<500 ; i++){
            System.out.println("\n");
        }
        System.out.flush();
    }
}
