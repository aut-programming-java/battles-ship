import java.util.Random;

/**
 * A player in general has two table and many ships
 * This abstract class has several common method
 *
 * @autor Alireza Mazochi
 * @since  97/01/6 (Happy new year!)
 * @version 1.0.0
 */
abstract public class Player {
    protected Ship ships[];
    public final static int NUM_OF_SHIPS=5;
    protected int tableOwn[][]=new int[10][10];
                                // 0--> empty
                                // 1--> Ship
                                // 2--> damaged Ship

    protected int tableEnemy[][]=new int[10][10];
                                  //0--> empty
                                 //1--> wrong attacked
                                 //2--> correct attacked
    protected boolean typeOfShoot;
                                //false--> exact
                                //true--> not exact

    /**
     * Constructor for class player
     * Every player needs several Ships
     */
    public Player(){
        ships=new Ship[NUM_OF_SHIPS];
        createShips();
    }

    /**
     * This method for creating ships in start of every games
     */
    abstract public void createShips();

    /**
     * monitor is a method that shows two table for the player
     * This method use color in tables :)
     */
    public void monitor(){
        clearConsole();
        System.out.print("  |");
        for(int i=0 ; i<=9 ; i++){
            System.out.print(" "+i+" |");
        }
        System.out.print("   ");
        System.out.print("  |");
        for(int i=0 ; i<=9 ; i++){
            System.out.print(" "+i+" |");
        }
        System.out.println();
        System.out.print("--|---+---+---+---+---+---+---+---+---+---+");
        System.out.print("   ");
        System.out.print("--|---+---+---+---+---+---+---+---+---+---+");
        System.out.println();
        for (int i = 0; i <= 9; i++) {
            System.out.print(i+" |");
            for (int j = 0; j <= 9; j++) {
                switch (tableEnemy[j][i]){
                    case 0:
                        System.out.print("   |");
                        break;
                    case 1:
                       // System.out.print(" X |");
                        System.out.print(Color.YELLOW_BACKGROUND_BRIGHT+"   "+Color.ANSI_RESET);
                        System.out.print("|");
                        break;
                    case 2:
                      //  System.out.print(" & |");
                        System.out.print(Color.GREEN_BACKGROUND_BRIGHT+"   "+Color.ANSI_RESET);
                        System.out.print("|");
                        break;
                }
            }
            System.out.print("   ");
            System.out.print(i+" |");
            for (int j = 0; j <= 9; j++) {
                switch (tableOwn[j][i]){
                    case 0:
                        System.out.print("   |");
                        break;
                    case 1:
                       // System.out.print(" @ |");
                        System.out.print(Color.BLUE_BACKGROUND_BRIGHT+"   "+Color.ANSI_RESET);
                        System.out.print("|");
                        break;
                    case 2:
                        //System.out.print(" # |");
                        System.out.print(Color.RED_BACKGROUND_BRIGHT+"   "+Color.ANSI_RESET);
                        System.out.print("|");
                        break;
                }            }
            System.out.println();
            System.out.print("--|---+---+---+---+---+---+---+---+---+---+");
            System.out.print("   ");
            System.out.print("--|---+---+---+---+---+---+---+---+---+---+");
            System.out.println();
        }
        System.out.println("=========================================================================================");
        System.out.print("=============== OPPONENT ==================");
        System.out.print("   ");
        System.out.print("================ PlAYER ===================");
        System.out.println();
        System.out.println("=========================================================================================");
    }


    /**
     * This method checks a input ship has not interference with other ships
     * @param ship new ship
     * @return if add new ship is OK or not
     */
    public boolean checkAddShip(Ship ship){
        if(ship.check()==true){
            boolean flag=true;
            for(int j=0 ; j<ship.getLength() ; j++){
                if(ship.getDirect()==true){
                    if(tableOwn[ship.getLocationX()][ship.getLocationY()+j]==1){
                        return false;
                    }
                }
                else{
                    if(tableOwn[ship.getLocationX()+j][ship.getLocationY()]==1){
                        return false;
                    }
                }
            }
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * When opponent attacks to the player we use this method
     * @param x x of the point of shoot
     * @param y y of the point of shoot
     * @return if enemy's attack is successful or not
     */
    public boolean shoot (int x,int y){
        for(int i=0 ; i<NUM_OF_SHIPS; i++){
            if(ships[i].shoot(x,y)==true){
                return true;
            }
        }
        return false;
    }

    /**
     * If a payer has at lest one ship that means has ship!
     * @return if the player has ship returns true
     */
    public boolean hasShip(){
        for(int i=0 ; i<NUM_OF_SHIPS ; i++){
            if(ships[i].isAvailable()==true){
                return true;
            }
        }
        return false;
    }

    /**
     * this method for attacking to enemy player
     * @param opponent the enemy player
     * @return if the player success return true
     */
    abstract public boolean attack(Player opponent);

    /**
     * A getter for tableOwn
     * @return tableOwn
     */
    public int[][] getTableOwn() {
        return tableOwn;
    }

    /**
     * A getter for tableEnemy
     * @return tableEnemy
     */
    public int[][] getTableEnemy() {
        return tableEnemy;
    }

    /**
     * A simple method for clearing console
     */
    public void clearConsole(){
        System.out.flush();
        for(int i = 0 ; i<500 ; i++){
            System.out.println("\n");
        }
        System.out.flush();
    }

    /**
     * A method for delay
     * @param time Amount of time that we like program delays
     */
    public void delay(int time){
        try
        {
            Thread.sleep(time);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }
}
