/**
 * This program is a famous game that  its name is Battle Ship
 * A game that two type;playing with together and playing with computer
 * This application provide a colorful console :)
 *
 * @author Alireza Mazochi
 * @since  97/01/6 (Happy new year!)
 * @version 1.0.0
 */

public class Main {
    /**
     * This method is starting point for this program
     * @param args input values that enter in command line
     */
    public static void main(String[] args){
        Game game=new Game();
        game.start();

    }
}
