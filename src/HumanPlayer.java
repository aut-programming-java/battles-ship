import java.util.Random;
import java.util.Scanner;

/**
 * This inheritance Class player
 * and for time we have a human player
 *
 * @autor Alireza Mazochi
 * @since  97/01/6 (Happy new year!)
 * @version 1.0.0
 */
public class HumanPlayer extends Player {

    /**
     * This method for creating ships in start of every games
     */
    @Override
    public void createShips() {
        Scanner scanner=new Scanner(System.in);
        int x;
        int y;
        int length;
        boolean direction;
        System.out.println("Enter type of your shoots(false--> exact / true --> not exact)");
        typeOfShoot=scanner.nextBoolean();
        for(int i=0 ; i<Player.NUM_OF_SHIPS ; i++){
            System.out.println("Enter x,y,length and direction( false--> horizontal / true-->Vertical ) of Ship #"+(i+1));
            x=scanner.nextInt();
            y=scanner.nextInt();
            length=scanner.nextInt();
            direction=scanner.nextBoolean();
            Ship ship=new Ship(direction,x,y,length);
            if(checkAddShip(ship)==false){
                System.out.println("ERROR::This ship don't created!");
                ship=null;
                i--;
            }
            else{
                ships[i]=ship;
                for(int j=0 ; j<length ; j++) {
                    if (direction == true) {
                        tableOwn[x][y+j]=1;
                    }
                    else {
                        tableOwn[x+j][y]=1;
                    }
                }
            }
        }
    }

    /**
     * this method for attacking to enemy player
     * @param opponent the enemy player
     * @return if the player success return true
     */
    @Override
    public boolean attack(Player opponent) {
        Scanner scanner=new Scanner(System.in);
        while(true){
            monitor();
            System.out.println("Enter x and y:");
            int x=scanner.nextInt();
            int y=scanner.nextInt();
            if(0<=x && x<=9 && 0<=y && y<=9 && tableEnemy[x][y]==0){
                int locationX;
                int locationY;
                while(true){
                    if(typeOfShoot){
                        Random random=new Random();
                        locationX=x+random.nextInt(3)-1;
                        locationY=y+random.nextInt(3)-1;
                        if(0<=locationX && locationX<=9 && 0<=locationY && locationY<=9 && tableEnemy[locationX][locationY]==0){
                            break;
                        }
                    }
                    else{
                        locationX=x;
                        locationY=y;
                        break;
                    }
                }
                if(opponent.shoot(locationX,locationY)==true){
                    tableEnemy[locationX][locationY]=2;
                    opponent.tableOwn[locationX][locationY]=2;
                    monitor();
                    System.out.println("Correct!");
                    delay(1000);
                    return true;
                }
                else{
                    tableEnemy[locationX][locationY]=1;
                    monitor();
                    System.out.println("Wrong!");
                    delay(1000);
                    return false;
                }
            }
            else{
                System.out.println("ERROR::This location choice before!");
                delay(1000);
            }
        }
    }
}
